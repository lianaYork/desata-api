'use strict';

/**
 * Favourite.js controller
 *
 * @description: A set of functions called "actions" for managing `Favourite`.
 */

module.exports = {

  /**
   * Retrieve favourite records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.favourite.search(ctx.query);
    } else {
      return strapi.services.favourite.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a favourite record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.favourite.fetch(ctx.params);
  },

  /**
   * Count favourite records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.favourite.count(ctx.query);
  },

  /**
   * Create a/an favourite record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.favourite.add(ctx.request.body);
  },

  /**
   * Update a/an favourite record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.favourite.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an favourite record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.favourite.remove(ctx.params);
  }
};
